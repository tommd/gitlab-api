-----------------------------------------------------------------------------
-- |
-- Module     : Gitlab
-- Copyright  : (c) Daniel Firth 2018
-- License    : BSD3
-- Maintainer : locallycompact@gmail.com
-- Stability  : experimental
--
-- This file defines the Gitlab API V4 API
-- (https://docs.gitlab.com/ce/api/)
--
-----------------------------------------------------------------------------

module Gitlab
  ( module Gitlab.Core
  , module Gitlab.Projects
  , module Gitlab.Wikis
  )
where

import           Gitlab.Core
import           Gitlab.Projects
import           Gitlab.Wikis
