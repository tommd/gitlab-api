{-# LANGUAGE TemplateHaskell #-}
-----------------------------------------------------------------------------
-- |
-- Module     : Gitlab.Projects
-- Copyright  : (c) Daniel Firth 2018
-- License    : BSD3
-- Maintainer : locallycompact@gmail.com
-- Stability  : experimental
--
-- This file defines the Gitlab API V4 Projects API
-- (https://docs.gitlab.com/ce/api/projects.html)
--
-----------------------------------------------------------------------------

module Gitlab.Projects
  ( -- * Data Types/Lenses
    GitlabCommitData(..)
  , glAuthoredDate
  , glAuthorEmail
  , glAuthorName
  , glCommittedDate
  , glCommitId

  -- * API
  , getCommitData
  )
where

import           Data.Yaml
import           Gitlab.Core
import           Lens.Micro.Platform
import           RIO
import           RIO.Time

data GitlabCommitData = GitlabCommitData {
  _glAuthoredDate  :: UTCTime,
  _glAuthorEmail   :: Text,
  _glAuthorName    :: Text,
  _glCommittedDate :: UTCTime,
  _glCommitterName :: Text,
  _glCommitId      :: Text
} deriving (Eq, Show)

$(makeLenses ''GitlabCommitData)

instance FromJSON GitlabCommitData where
  parseJSON = withObject "GitlabCommitData" $ \v -> GitlabCommitData
    <$> v .: "authored_date"
    <*> v .: "author_email"
    <*> v .: "author_name"
    <*> v .: "committed_date"
    <*> v .: "committer_name"
    <*> v .: "id"

getCommitData :: MonadGitlab env m => Text -> Text -> m GitlabCommitData
getCommitData p r = gitlabRequest
  "GET"
  ("/projects/" <> rParam p <> "/repository/commits/" <> rParam r)
  mempty
